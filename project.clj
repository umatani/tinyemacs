(defproject tinyemacs "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 ;[clojure-lanterna "0.9.4"]
                 ]
  :resource-paths
  ["/Users/umatani/git/lanterna/target/lanterna-3.0.0-SNAPSHOT.jar"]
  :main ^:skip-aot tinyemacs.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
