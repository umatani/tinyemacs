<link href="http://kevinburke.bitbucket.org/markdowncss/markdown.css" rel="stylesheet"></link>

# tinyemacs

FIXME: description

## Installation

Download from http://example.com/FIXME.

## Usage

FIXME: explanation

    $ java -jar tinyemacs-0.1.0-standalone.jar [args]

## Options

FIXME: listing of options this app accepts.

## Examples

...

### Internals

[0. 基本入出力]
* Core(Main)
* Window
* KeyListener

gitの使い方も教える．課題提出時点でタグ．

[1. キー操作]
* カーソルの追加とput-char拡張
* KeyMap
  * C-p, C-f, C-b, C-n
  * C-a, C-e
  * M-<, M->

[2. バッファとマルチウィンドウ]
* WindowWriter
* Buffer
  * Windowへの操作ではなく，Buffer中のデータ編集がWindowに自動反映
  * C-p, C-f, C-b, C-n でスクロール．C-l
  * 多段キーマップもここ．C-x 2, C-x o, C-x 1, C-g
* InfoArea
  * split window (windowの仮想化)はこの段階で
* Cursor
  * Windowじゃなく，Bufferにひもづける

[3. 編集機能]
* Mark
  * C-@, C-k, C-y, C-w
  * C-s, C-r, M-%

[4. ファイル操作とキーストローク]
* File
  - TextFile
* MiniBuffer <: Buffer
   * ファイル入出力 C-x C-f
* Config

[5. メジャーモード]
* PlaneBuffer <: Buffer
* Table
* CSVFile <: File

[5] は，代わりに Mailer というのもあり
[5] は，代わりに etags (M-., M-,)というのもあり


### Bugs

...

### Any Other Sections
### That You Think
### Might be Useful

## License

Copyright © 2015 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
