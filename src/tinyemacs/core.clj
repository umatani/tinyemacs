(ns tinyemacs.core
  ;(require [lanterna.terminal :as t])
  ;(require [lanterna.screen :as s])
  (:require (tinyemacs [window :as win] [key :as key]))
  (:gen-class))

(def ^:const win-width  80)
(def ^:const win-height 25)

(defn -main
  "Starts a tiny emacs.
  With some arg(s), it runs in CUI."
  [& args]
  (let [[w width height] (win/make-window win-width
                                          win-height
                                          (pos? (count args)))
        kl (key/make-listener w)]
    (try
      (win/start w)
      ;;(println "(x,y):" width height)

      (loop [k (key/read-key kl)
             i 0]
        ;;(println "key:" k)
        (when-not (= k \q)
          (win/put-char w (rem i width) (quot i width) k)
          (win/refresh w)
          (recur (key/read-key kl) (inc i))))

      (finally (win/stop w)))))
