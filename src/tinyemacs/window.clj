(ns tinyemacs.window
  (:import
   (java.util EnumSet)
   (java.awt Font)
   (com.googlecode.lanterna
    SGR TextCharacter TerminalPosition TerminalSize TextColor$ANSI)
   (com.googlecode.lanterna.terminal DefaultTerminalFactory Terminal)
   (com.googlecode.lanterna.screen TerminalScreen)
   (com.googlecode.lanterna.terminal.swing
    SwingTerminalFontConfiguration
    SwingTerminalDeviceConfiguration
    SwingTerminalDeviceConfiguration$CursorStyle))
  (:gen-class))

(defn- make-char [c]
  (TextCharacter. c
                  TextColor$ANSI/DEFAULT
                  TextColor$ANSI/DEFAULT
                  (EnumSet/noneOf SGR)
                  #_(EnumSet/of SGR/REVERSE)))

(defn- make-pos [col row]
  (TerminalPosition. col row))

(defn put-char
  "Puts a character C at position (X,Y) in W."
  [w x y c]
  (.setCharacter w (make-pos x y) (make-char c)))

(defn make-window
  "Makes a new window of size W x H."
  [w h is-cui]
  (let [fonts (make-array Font 1)
        _ (aset fonts 0 (Font. Font/MONOSPACED Font/PLAIN 24))
        font-cnf (SwingTerminalFontConfiguration/newInstance fonts)
        dev-cnf (SwingTerminalDeviceConfiguration.
                 160 500
                 SwingTerminalDeviceConfiguration$CursorStyle/REVERSED
                 TextColor$ANSI/CYAN true)
        t-factory (-> (DefaultTerminalFactory.)
                      (.setSuppressSwingTerminalFrame is-cui)
                      (.setSwingTerminalFrameDeviceConfiguration dev-cnf)
                      (.setSwingTerminalFrameFontConfiguration font-cnf)
                      (.setSwingTerminalFrameTitle "Tiny Emacs"))
        _ (.setInitialTerminalSize t-factory (TerminalSize. w h))
        w (-> (.createTerminal t-factory)
              TerminalScreen.)
        w-size (.getTerminalSize w)]
    [w (.getColumns w-size) (.getRows w-size)]))

(defn start
  "Starts window W."
  [w]
  (.startScreen w))

(defn stop
  "Stops window W."
  [w]
  (.stopScreen w))

(defn refresh
  "Refreshs window W."
  [w]
  (.refresh w))
