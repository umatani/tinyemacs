(ns tinyemacs.key
  (:import (com.googlecode.lanterna.screen TerminalScreen)
           (com.googlecode.lanterna.input KeyStroke))
  (:gen-class))

(defn make-listener
  "Makes a new key listener attached to W."
  [w]
  {:type :key-listener :window w})

(defn read-key
  "Reads a key event from key listner KL.
  It blocks if no key event is ready."
  [kl]
  (let [k (.readInput (:window kl))]
    (.getCharacter k)))
